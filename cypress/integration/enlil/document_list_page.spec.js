/// <reference types="cypress" />

context('Enlil', () => {
    beforeEach(() => {
      cy.visit('https://demo.enlil.io')
    })

    //Assert and verify Document List page
it('Document list page', () => {
    cy.get('#LoginFormPresenter-email')
      .type('zarko.stepanov10@gmail.com')
      .should('have.value', 'zarko.stepanov10@gmail.com')
    cy.get('#LoginFormPresenter-password') 
      .type('Shubham##99')
    cy.get('#LoginFormPresenter-submit').click()  

    
    cy.get('#switchCompanyHeader')
      .contains('Switch Company')    
    cy.contains('Select a company below to sign-in to')
    //Click on company QA Zarko
    cy.get('button').contains('QA Zarko').click()
    //.wait(3000)

    //Document List
    cy.contains('Document List').should('have.attr', 'href','/document_revision').click()
    .wait(1000)
    
    //Content page
    cy.get('.Toastify')
    //Clear button 
    cy.get('#ListPresenter-clearButton').contains('CLEAR FILTERS').and('not.be.disabled')
    //Records found
    cy.contains('Records found:')
    cy.get('[data-testid="numberOfRecords"]').and('be.visible')
    //Create A Document button
    cy.get('#ListPresenter-create').contains('CREATE A DOCUMENT')
    .and('not.be.disabled')

    //Table
    cy.get('.ReactVirtualized__Table__headerRow ').should('be.visible')
    //DOC ID column
    cy.get('[aria-label="docId"]').contains('DOC ID')
    cy.get('#docId').should('be.visible')
    
    //VERSION column
    cy.contains('Version')
    cy.get('#version').should('be.visible')
    
    //DOCUMENT TITLE
    cy.contains('Document title')
    cy.get('#name').should('be.visible')

    //TYPE
    cy.contains('Type')
    cy.get('#mui-component-select-documentTypeId').should('be.visible')

    //OWNER
    cy.contains('Owner')
    cy.get('#owner').should('be.visible')
    
    //CR ID
    cy.contains('CR ID')
    cy.get('#crId').should('be.visible')

    //STATUS
    cy.contains('Status')
    cy.get('[id="mui-component-select-status[]"]').contains('Released').should('be.visible').click()
        

    //Dropdown STATUS
    cy.get('[role="listbox"]')
    cy.get('.Mui-selected').contains('Released').should('be.visible')
    //cy.get(['data-value="RELEASED"']).contains('Released').should('be.visible')
    cy.get('em').contains('None').should('be.visible')
    cy.get('[role="option"]').contains('None').should('be.visible')
    cy.get('[data-value="DRAFT"]').contains('Draft').should('be.visible')
    cy.get('[data-value="IN_REVIEW"]').contains('In Review').should('be.visible')
    cy.get('[data-value="OBSOLETE"]').contains('Obsolete').should('be.visible')
    cy.get('[data-value="PENDING_CHANGE"]').contains('Pending Change').should('be.visible')
    //.wait(2000)
    //cy.contains('Document List').should('have.attr', 'href','/document_revision').click().click()
    
    //CLOSE DROPDOWN
    cy.get('.Mui-selected').click()

    //RELEASED DATE
    cy.get('#mui-component-select-releasedAfter').click()
    cy.get('.jss665').contains('Released Date')
    cy.get('#mui-component-select-releasedAfter')
    cy.get('em').contains('None')
    cy.contains('Last 7 days').should('be.visible')
    //cy.get('[data-value="2020-05-19T22:00:00.000Z"]').contains('Last 7 days')
    cy.contains('Last 14 days').should('be.visible')
    cy.contains('Last 30 days').should('be.visible')

    //CLOSE DROPDOWN DATE 
    cy.get('em').click()

  })
})