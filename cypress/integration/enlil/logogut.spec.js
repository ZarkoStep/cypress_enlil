/// <reference types="cypress" />

context('Enlil', () => {
    beforeEach(() => {
      cy.visit('https://demo.enlil.io')
    })

//Navigation menu
it('Navigation menu', () => {
    //Type email into imput field
    cy.get('#LoginFormPresenter-email')
      .type('zarko.stepanov10@gmail.com')
      .should('have.value', 'zarko.stepanov10@gmail.com')
      .screenshot()
    //Type password into imput field  
    cy.get('#LoginFormPresenter-password') 
      .type('Shubham##99')
      .screenshot()
    //Click button sign in  
    cy.get('#LoginFormPresenter-submit').click()  
    //Companies
    cy.get('#switchCompanyHeader')
      .contains('Switch Company')    
    cy.contains('Select a company below to sign-in to')
    cy.screenshot()
    //Click on company QA Zarko
    cy.get('button').contains('QA Zarko').click()
    
    //Logout

    //Click on the menu
    cy.get('#UserMenuButton').click()
    cy.screenshot()

    //Assert menu and click on logout
    cy.contains('My profile')
    cy.contains('Switch Company')
    cy.contains('Logout').click()

    //Assertation to be sure it user has properly logout
    cy.get('img').then(($el) => {
        Cypress.dom.isVisible($el) // true
    })
    //Assert elemtent on login page
    cy.document().should('have.property', 'charset').and('eq','UTF-8')
    cy.get('#root').should('be.visible')      
    cy.get('#LoginFormPresenter-title').contains('Sign in to Enlil')
    cy.contains('h4','Sign in to Enlil')
    cy.contains('EMAIL ADDRESS')
    cy.get('[name="email"]').and('be.enabled').and('be.visible')
    cy.get('#LoginFormPresenter-forgotLink').contains('Forgot your password?')
    cy.contains('PASSWORD')
    cy.get('[name="password"]').and('be.enabled').and('be.visible')
    cy.contains("Click here to register")
      .should('have.attr', 'href','/signup')
    })    
})